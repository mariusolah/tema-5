let input1Val, input2Val, input3Val, input4Val;
let input1, input2, input3, input4;

const eventFunction = () => {
    setInterval(() => {
        input1 = $('#input1');
        input2 = $('#input2');
        input3 = $('#input3');
        input4 = $('#input4');

        input1Val = $('#input1').val();
        input2Val = $('#input2').val();
        input3Val = $('#input3').val();
        input4Val = $('#input4').val();
    }, 1);

    document.querySelector('#col-1').addEventListener('click', event => {
        changeColor(input1, input2, input3, input4, event, input1Val, input2Val, input3Val, input4Val);
    });

    document.querySelector('#col-2').addEventListener('click', event => {
        changeColor(input2, input1, input3, input4, event, input2Val, input1Val, input3Val, input4Val);
    });

    document.querySelector('#col-3').addEventListener('click', event => {
        changeColor(input3, input2, input1, input4, event, input3Val, input2Val, input1Val, input4Val);
    });

    document.querySelector('#col-4').addEventListener('click', event => {
        changeColor(input4, input2, input3, input1, event, input4Val, input2Val, input3Val, input1Val);
    });
}

const changeColor = (in1, in2, in3, in4, e, in1V, in2V, in3V, in4V) => {
    $(in1).css('background-color', e.target.id)
    if (in1V == in2V) {
        $(in2).css('background-color', e.target.id)
    }
    if (in1V == in3V) {
        $(in3).css('background-color', e.target.id)
    }
    if (in1V == in4V) {
        $(in4).css('background-color', e.target.id)
    }
}

eventFunction();

